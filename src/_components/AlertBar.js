import React from 'react';
import { Snackbar, SnackbarContent, IconButton } from '@material-ui/core';

class AlertBar extends React.Component {
    state = {
        open: true,
    };

    handleRequestClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ open: false });
        this.props.clearAlert();
    };

    render() {
        return (
            <Snackbar anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
                open={this.state.open}
                autoHideDuration={3000}
                onClose={this.handleRequestClose} >
                <SnackbarContent
                    message={<span id="client-snackbar"> {this.props.message} </span>}
                    action={[
                        <IconButton key='close'
                            aria-label='Close'
                            color='inherit'
                            onClick={this.handleRequestClose}
                        />
                    ]}
                />
            </Snackbar>
        );
    }
}

export { AlertBar };