import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRouteRider = ({ component: Component, ...rest }) => (    // assign the component from props to Component and the rest of the props to rest
    <Route {...rest} render={props => (
            localStorage.getItem('user')
                ? JSON.parse(localStorage.getItem('user')).role === 'R'
                    ? <Component {...props} /> : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
                : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)