import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { App } from './App';
import * as serviceWorker from './serviceWorker';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

import { store, persistor } from './_helpers';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

const theme = createMuiTheme({
    typography: {
        useNextVariants: true,  // use next variants because typogrpahy v1 will become deprecated in the next update to muitheme
      },
});

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
