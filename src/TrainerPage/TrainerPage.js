import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { trainerActions } from '../_actions';
import { MyRiders } from './index';
import { PersistentDrawer } from './_components';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

class TrainerPage extends React.Component {
    componentDidMount() {
        this.props.trainerActions.pushScreenComponent(<MyRiders />);
    }

    getRides = event => {
        this.props.dispatch(trainerActions.getRides());
    }

    render() {
        const { user, trainer } = this.props;
        return (
            <div>
                <AppBar style={{ position: "fixed", left: 0, top: 0 }}>
                    <Toolbar>
                        <Typography variant="h5" color={"textPrimary"} noWrap>
                            You are logged in as {user.username}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className="row">
                    <div className="col-sm-3 col-md-2" style={{ height: 'calc(100% - 64px)', top: 64, }}>
                        <PersistentDrawer pushScreenComponent={this.props.trainerActions.pushScreenComponent} />
                    </div>
                    <div className="col-sm-9 col-md-9 col-md-offset-1" style={{ height: 'calc(100% - 64px)', top: 64, }}>
                        {trainer.screenComponent}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { trainer } = state;
    const { user } = state.authentication;
    return {
        trainer,
        user
    };
}

function mapDispatchToProps(dispatch) {
    return { trainerActions: bindActionCreators(trainerActions, dispatch) }
}

const connectedTrainerPage = connect(mapStateToProps, mapDispatchToProps)(TrainerPage);
export { connectedTrainerPage as TrainerPage };