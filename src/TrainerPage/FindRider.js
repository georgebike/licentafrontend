import React from 'react';

import { trainerActions } from '../_actions';

import { Button, ListItemText, Typography, List, ListItem } from '@material-ui/core';
import { connect } from 'react-redux';

class FindRider extends React.Component {
    componentDidMount() {
        const { trainer } = this.props

        if (!trainer.allRiders) {
            this.props.dispatch(trainerActions.getAllRiders());
        }
    }

    beginCollab(user_id, e) {
        // console.log(user_id);
        this.props.dispatch(trainerActions.beginCollab(user_id));
    }

    render() {
        const { trainer } = this.props;
        return(
            <div>
                {trainer.allRiders &&
                    <List style={{width: '100%', maxWidth: 720}}>
                        {trainer.allRiders.map((rider, index) =>
                            <ListItem key={index} className="jumbotron" alignItems="flex-start">
                                <ListItemText primary={`${rider.first_name} ${rider.last_name}`}
                                    secondary={
                                        <React.Fragment>
                                            <Typography component="span" style={{display: 'inline'}} color="textPrimary">
                                            <b>DISCIPLINE: </b>{rider.discipline} <b>LEVEL: </b>{rider.level}
                                            </Typography> <br />
                                            <b>BIKE: </b>{rider.bike} <b>AGE: </b>{rider.age} <b>WEIGHT: </b>{rider.weight} <b>HEIGHT: </b>{rider.height}
                                        </React.Fragment>
                                    }
                                />
                                <Button color="primary" onClick={this.beginCollab.bind(this, rider.user_id)} > Connect </Button>
                            </ListItem>
                        )}
                    </List>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { trainer } = state;

    return {
        trainer
    };
}

const connectedFindRider = connect(mapStateToProps)(FindRider);
export { connectedFindRider as FindRider };