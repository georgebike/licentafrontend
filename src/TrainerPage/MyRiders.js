import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { trainerActions } from '../_actions';
import { Button } from '@material-ui/core';

class MyRiders extends React.Component {
    componentDidMount() {
        const { trainer } = this.props

        if (!trainer.connectedRiders) {
            this.props.dispatch(trainerActions.getConnectedRiders());
        }
    }

    getRides (user_id, e) {
        this.props.dispatch(trainerActions.getRides(user_id));
    }

    endCollab (user_id, e) {
        this.props.dispatch(trainerActions.endCollab(user_id));
    }

    render() {
        const { user, trainer } = this.props;
        return (
            <div >
                <h1>Hi {user.username}!</h1>
                <p>Here you can find all the riders you're connected with, as well as their past rides</p>
                <h3>All Connected Riders:</h3>
                {trainer.connectedRiders &&
                    <ul>
                        {trainer.connectedRiders.map((rider, index) =>
                            <li key={rider.user_id} >
                                <Link to={{ pathname: "/monitoring", state: { riderTopic: rider.topic, rideId: null } }} className="btn btn-link">
                                    {rider.first_name} {rider.last_name}
                                </Link>
                                <Button color="primary" onClick={this.getRides.bind(this, rider.user_id)} > Get Rides </Button>
                                <Button color="primary" onClick={this.endCollab.bind(this, rider.user_id)} > End Collaboration </Button>
                            </li>
                        )}
                    </ul>
                }
                <h3>Rider's rides</h3>
                {trainer.loading && <em>Loading rides... </em>}
                {trainer.rides &&
                    <ul>
                        {trainer.rides.map((ride, index) =>
                            <li key={ride.id}>
                                <Link to={{ pathname: "/monitoring", state: { rideId: ride.id } }} className="btn btn-link">
                                    Ride id: {ride.id}  From: {ride.date}
                                </Link>
                            </li>
                        )}
                    </ul>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { trainer, authentication } = state;
    const { user } = authentication;
    return {
        user,
        trainer
    };
}

const connectedMyRiders = connect(mapStateToProps)(MyRiders);
export { connectedMyRiders as MyRiders }
