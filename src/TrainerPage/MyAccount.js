import React from 'react';
import { connect } from 'react-redux';

import { userActions } from '../_actions';
import { Button, ListItemText, List, ListItem, TextField } from '@material-ui/core';

class MyAccount extends React.Component {
    constructor(props) {
        super(props);

        const { user_info } = this.props;

        this.state = { ...user_info };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(userActions.get_info());
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    handleSubmit(event) {
        event.preventDefault();
        var user_info = this.state;
        this.props.dispatch(userActions.update(user_info));
    }

    handleDelete(event) {
        event.preventDefault();
        this.props.dispatch(userActions.delete());
    }

    render() {
        const { user } = this.props;
        var { user_info } = this.props;
        return (
            <div>
                <h1>Hello {user.username}!</h1>
                <h3>View and modify your user account details</h3>
                {user_info &&
                    <List style={{ width: '100%', maxWidth: 720 }}>
                        {Object.keys(user_info).map((key, index) => {
                            if (key !== "id" && key !== "user_id") return (
                                <ListItem key={index} className="jumbotron" alignItems="flex-start">
                                    <ListItemText primary={`${key}`} />
                                    <TextField label={`Change your ${key}`} placeholder={`${key}`} name={`${key}`} defaultValue={`${user_info[key]}`} fullWidth={true}
                                                style={{ margin: 15, position: "relative" }} onChange={this.handleChange} />
                                </ListItem>); return null;
                        }
                        )}
                    </List>
                }
                <Button color="primary" onClick={this.handleSubmit} > UPDATE </Button>
                <Button color="secondary" onClick={this.handleDelete}> DELETE ACCOUNT </Button>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { authentication } = state;
    const { user, user_info } = authentication;
    return {
        user,
        user_info,
    };
}

const connectedMyAccount = connect(mapStateToProps)(MyAccount);
export { connectedMyAccount as MyAccount };