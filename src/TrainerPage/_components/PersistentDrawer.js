import React from 'react';
import { Link } from 'react-router-dom';

import { Drawer } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';

import { MyRiders, MyAccount, FindRider } from '../index';

const styles = theme => ({
    drawer: {
        width: "15%",
        height: 'calc(100% - 64px)',
        top: 64,
        flexShrink: 0,
    },
    drawerPaper: {
        width: "15%",
        height: 'calc(100% - 64px)',
        top: 64,
    }
});

class PersistentDrawer extends React.Component {
    constructor(props){
        super(props);

        this.triggerMyRidersComponent = this.triggerMyRidersComponent.bind(this);
        this.triggerFindRiderComponent = this.triggerFindRiderComponent.bind(this);
        this.triggerMyAccountComponent = this.triggerMyAccountComponent.bind(this);
    }
    
    triggerMyRidersComponent(){
        this.props.pushScreenComponent(<MyRiders />)
    }
    triggerFindRiderComponent(){
        this.props.pushScreenComponent(<FindRider />)
    }
    triggerMyAccountComponent(){
        this.props.pushScreenComponent(<MyAccount />)
    }

    render() {
        const { classes } = this.props;
        return (
            <Drawer className={classes.drawer} variant="persistent" anchor="left" open={true} classes={{ paper: classes.drawerPaper, }}>
                <List>
                    <ListItem button key="riders" onClick={this.triggerMyRidersComponent}>
                        <ListItemIcon><InboxIcon /></ListItemIcon>
                        <ListItemText primary="My riders" />
                    </ListItem>
                    <ListItem button key="trainer" onClick={this.triggerFindRiderComponent}>
                        <ListItemIcon><InboxIcon /></ListItemIcon>
                        <ListItemText primary="Find Rider" />
                    </ListItem>
                    <ListItem button key="account" onClick={this.triggerMyAccountComponent}>
                        <ListItemIcon><InboxIcon /></ListItemIcon>
                        <ListItemText primary="My account" />
                    </ListItem>
                    <ListItem button component={Link} to="/login" key="logout">
                        <ListItemText primary="Logout" />
                    </ListItem>
                </List>
            </Drawer>
        );
    }
}
let styledDrawer = withStyles(styles)(PersistentDrawer);
export { styledDrawer as PersistentDrawer }