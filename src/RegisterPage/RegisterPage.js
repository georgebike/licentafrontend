import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';

import { FormControl, FormLabel, RadioGroup, FormControlLabel, TextField, Button, Radio, FormHelperText } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

class RegisterPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                username: '',
                password: '',
                role: ''
            },
            rider_info: {
                first_name: '',
                last_name: '',
                age: 0,
                weight: 0,
                height: 0,
                discipline: '',
                level: '',
                bike: '',
                topic: '',
            },
            trainer_info: {
                first_name: '',
                last_name: '',
                discipline: '',
                level: '',
                certificate: ''
            },
            submitted: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleTrainerInfoChange = this.handleTrainerInfoChange.bind(this);
        this.handleRiderInfoChange = this.handleRiderInfoChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    handleTrainerInfoChange(event) {
        const { name, value } = event.target;
        const { trainer_info } = this.state;

        this.setState({
            trainer_info: {
                ...trainer_info,
                [name]: value
            }
        });
    }

    handleRiderInfoChange(event) {
        const { name, value } = event.target;
        const { rider_info } = this.state;

        if (name === 'age' || name === 'weight' || name === 'height') {
            var parsedInt = parseInt(value);
            if (parsedInt) {
                this.setState({
                    rider_info: {
                        ...rider_info,
                        [name]: parsedInt
                    }
                });
            }
        } else {
            this.setState({
                rider_info: {
                    ...rider_info,
                    [name]: value
                }
            });
        }
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true })
        const { user } = this.state;
        var user_info = null;
        if (user.role === "T") {
            user_info = this.state.trainer_info
        } else if (user.role === "R") {
            user_info = this.state.rider_info
        }

        const { dispatch } = this.props;
        if (user.username && user.password && user_info.first_name && user_info.last_name && user_info.discipline && user_info.level) {
            if (user.role === 'R' && user_info.topic && user_info.age>0 && user_info.weight>0 && user_info.height>0) {
                dispatch(userActions.register(user, user_info));
            } else if (user.role === 'T') {
                dispatch(userActions.register(user, user_info));
            }
        }
    }

    render() {
        const { user, rider_info, trainer_info, submitted } = this.state;
        return (
            <div className="container-fluid">
                <AppBar style={{ position: "fixed", left: 0, top: 0 }}>
                    <Toolbar>
                        <Typography variant="h5" color={"textPrimary"} noWrap>
                            Bike training utility tool - REGISTER
                         </Typography>
                    </Toolbar>
                </AppBar>

                <div className="col-md-4 offset-md-4" style={{ height: 'calc(100% - 128px)', top: 128, }}>
                    <TextField required
                        label="Enter your Username"
                        placeholder="Username"
                        name="username"
                        value={user.username}
                        fullWidth={true}
                        style={{ margin: 15, position: "relative" }}
                        error={submitted && !user.username}
                        helperText={submitted && !user.username && "Required Field"}
                        onChange={this.handleChange} />
                    <TextField required
                        id="standard-password-input"
                        label="Enter your Password"
                        placeholder="Password"
                        type="password"
                        name="password"
                        value={user.password}
                        fullWidth={true}
                        style={{ margin: 15, position: "relative" }}
                        error={submitted && !user.password}
                        helperText={submitted && !user.password && "Required Field"}
                        onChange={this.handleChange} />
                    <FormControl required error={submitted && !user.role} component="fieldset" style={{ margin: 15, position: "relative" }}>
                        <FormLabel component="legend">Please select your role:</FormLabel>
                        <RadioGroup row
                            aria-label="Role"
                            name="role"
                            value={this.state.value}
                            onChange={this.handleChange}>
                            <FormControlLabel value="R" control={<Radio color="primary" />} label="Rider" />
                            <FormControlLabel value="T" control={<Radio color="primary" />} label="Trainer" />
                            {submitted && !user.role && <FormHelperText>Please select a role</FormHelperText>}
                        </RadioGroup>
                    </FormControl>

                    {user.role === 'T' &&
                        <div>
                            <TextField required label="Enter your first name" placeholder="First Name" name="first_name" value={trainer_info.first_name} fullWidth={true}
                                style={{ margin: 15, position: "relative" }} error={submitted && !trainer_info.first_name} helperText={submitted && !trainer_info.first_name && "Required Field"}
                                onChange={this.handleTrainerInfoChange} />
                            <TextField required label="Enter your last name" placeholder="Last Name" name="last_name" value={trainer_info.last_name} fullWidth={true}
                                style={{ margin: 15, position: "relative" }} error={submitted && !trainer_info.last_name} helperText={submitted && !trainer_info.last_name && "Required Field"}
                                onChange={this.handleTrainerInfoChange} />
                            <TextField required label="Enter your discipline" placeholder="(MTB/Road/Gravel etc.)" name="discipline" value={trainer_info.discipline} fullWidth={true}
                                style={{ margin: 15, position: "relative" }} error={submitted && !trainer_info.discipline} helperText={submitted && !trainer_info.discipline && "Required Field"}
                                onChange={this.handleTrainerInfoChange} />
                            <TextField required label="Enter your level" placeholder="(Beginner/Intermediate/Advanced)" name="level" value={trainer_info.level} fullWidth={true}
                                style={{ margin: 15, position: "relative" }} error={submitted && !trainer_info.level} helperText={submitted && !trainer_info.level && "Required Field"}
                                onChange={this.handleTrainerInfoChange} />
                            <TextField label="Enter your certificate" placeholder="Certificate ID" name="certificate" value={trainer_info.certificate} fullWidth={true}
                                style={{ margin: 15, position: "relative" }}
                                onChange={this.handleTrainerInfoChange} />
                        </div>
                    }
                    {user.role === 'R' &&
                        <div>
                            <TextField required label="Enter your first name" placeholder="First Name" name="first_name" value={rider_info.first_name} fullWidth={true}
                                style={{ margin: 15, position: "relative" }} error={submitted && !rider_info.first_name} helperText={submitted && !rider_info.first_name && "Required Field"}
                                onChange={this.handleRiderInfoChange} />
                            <TextField required label="Enter your last name" placeholder="Last Name" name="last_name" value={rider_info.last_name} fullWidth={true}
                                style={{ margin: 15, position: "relative" }} error={submitted && !rider_info.last_name} helperText={submitted && !rider_info.last_name && "Required Field"}
                                onChange={this.handleRiderInfoChange} />
                            <TextField required label="Enter your discipline" placeholder="(MTB/Road/Gravel etc.)" name="discipline" value={rider_info.discipline} fullWidth={true}
                                style={{ margin: 15, position: "relative" }} error={submitted && !rider_info.discipline} helperText={submitted && !rider_info.discipline && "Required Field"}
                                onChange={this.handleRiderInfoChange} />
                            <TextField required label="Enter your level" placeholder="(Beginner/Intermediate/Advanced)" name="level" value={rider_info.level} fullWidth={true}
                                style={{ margin: 15, position: "relative" }} error={submitted && !rider_info.level} helperText={submitted && !rider_info.level && "Required Field"}
                                onChange={this.handleRiderInfoChange} />
                            <TextField required label="Enter the bike's MQTT Topic" placeholder="Topic" name="topic" value={rider_info.topic} fullWidth={true}
                                style={{ margin: 15, position: "relative" }} error={submitted && !rider_info.topic} helperText={submitted && !rider_info.topic && "Required Field"}
                                onChange={this.handleRiderInfoChange} />
                            <TextField label="Enter your age" placeholder="Age" name="age" value={rider_info.age} fullWidth={true} type="number" step={1}
                                style={{ margin: 15, position: "relative" }} error={submitted && rider_info.age<0} helperText={submitted && rider_info.age<0 && "Age must be positive integer"}
                                onChange={this.handleRiderInfoChange} />
                            <TextField label="Enter your weight" placeholder="Weight" name="weight" value={rider_info.weight} fullWidth={true} type="number" step={1}
                                style={{ margin: 15, position: "relative" }} error={submitted && rider_info.weight<0} helperText={submitted && rider_info.weight<0 && "Weight must be positive integer"}
                                onChange={this.handleRiderInfoChange} />
                            <TextField label="Enter your height" placeholder="Height" name="height" value={rider_info.height} fullWidth={true} type="number" step={1}
                                style={{ margin: 15, position: "relative" }} error={submitted && rider_info.height<0} helperText={submitted && rider_info.height<0 && "Height must be positive integer"}
                                onChange={this.handleRiderInfoChange} />
                            <TextField label="Enter your bike" placeholder="Bike" name="bike" value={rider_info.bike} fullWidth={true}
                                style={{ margin: 15, position: "relative" }}
                                onChange={this.handleRiderInfoChange} />
                        </div>
                    }
                    <br />
                    <Button variant="contained" color="primary" style={{ left: 15, top: 0, position: "relative" }} onClick={this.handleSubmit}>Register</Button>
                    <Link to="/login" style={{ margin: 15, left: 30, position: "relative" }} className="btn btn-link">Cancel</Link>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedRegisterPage = connect(mapStateToProps)(RegisterPage);
export { connectedRegisterPage as RegisterPage };