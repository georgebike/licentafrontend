import React from 'react';
import { compose, withProps } from 'recompose';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";
var CircularBuffer = require("circular-buffer");

const MapComponent = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBWTc6vEpPAJ1ty2tp0So-HUirKTCZIuDk&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{
            position: 'absolute',
            height: `470px`,
            width: `100%`,
            justifyContent: 'flex-end',
            alignItems: 'center',
        }} />,
        mapElement: <div style={{
            position: 'absolute',
            height: `470px`,
            width: `100%`,
        }} />,
    }),
    withScriptjs,
    withGoogleMap
)((props) =>
    <GoogleMap
        defaultZoom={14}
        defaultCenter={{ lat: 46.756856, lng: 23.595981 }} >
        {props.markers}
    </GoogleMap>
);

class MapContainer extends React.Component {
    constructor(props) {
        super(props);
        if(this.props.rideSelected){
            console.log(this.props.positionArray);
            this.positionArray = this.props.positionArray;
        } else {
            this.coordsBuffer = CircularBuffer(5)
            this.position = {lat: -1, lng: -1}
        }
    }

    componentDidUpdate() {
        if(!this.props.rideSelected){
            this.position = {lat: this.props.lat, lng: this.props.lng};
            this.coordsBuffer.push(this.position);
        } else if (this.props.rideSelected) {
            this.positionArray = this.props.positionArray;
        }
    }

    render() {
        if(this.props.rideSelected){
            var markers = this.positionArray.map((position, index) => <Marker key={index} position={position.position} title={position.time} />)
        } else {
            markers = this.coordsBuffer.toarray().map((position, index) => <Marker key={index} position={position} />)
        }
        return (
            <MapComponent markers={markers} />
        );
    }
}

export { MapContainer };