import React from 'react';
import Highcharts from 'highcharts/highstock';
import HighchartsReact from 'highcharts-react-official';

var CircularBuffer = require("circular-buffer");

class CreateHighChart extends React.Component {
    constructor(props) {
        super(props);

        this.buff = new CircularBuffer(10);     // Circular buffer for displaying 10 values on the chart all the time

        this.state = {
            data: this.props.data,
            rideSelected: this.props.rideSelected
        }
    }

    componentWillReceiveProps(props) {
        if(props.rideSelected && (props.data !== this.state.data)) {
            this.setState({ data: props.data });
        }
    }

    componentDidUpdate() {
        if (!this.state.rideSelected && this.props.data[1] !== undefined){
            this.buff.push(this.props.data);
        }
    }

    render() {
        var showData = this.state.data;
        var navigatorEnabled = true;
        if (!this.state.rideSelected) {
            showData = this.buff.toarray();
            navigatorEnabled = false;
        }
        const options = {
            chart: {
                animation: {
                    duration: 1000,
                }
            },

            rangeSelector: {
                selected: 2,
                inputEnabled: false,
                buttonTheme: {
                    visibility: 'hidden'
                },
                labelStyle: {
                    visibility: 'hidden'
                }
            },
            
            navigator: {
                enabled: navigatorEnabled,
                xAxis: {
                    labels: {
                        enabled: false,
                        autoRotation: 10
                    }
                }
            },
    
            title: {
                text: null
            },

            credits: {
                enabled: false
            },

            tooltip: {
                formatter: function () {
                    var point = this.points[0];
                    return '<b>' + point.series.name + '</b><br/>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '<br/>' +
                        + Highcharts.numberFormat(point.y, 2);
                },
                shared: true
            },

            xAxis: {
                gridLineWidth: 1,
                labels: {   
                    formatter: function () {return Highcharts.dateFormat('%H:%M:%S', this.value)} 
                },
            },

            yAxis: {
                gridLineWidth: 1,
                labels: {
                    enabled: false
                },
                title: {
                    text: null
                },
            },
    
            series: [{
                name: "Value",
                data: showData,
                tooltip: {
                    valueDecimals: 2
                },
                dataLabels: {
                    enabled: true,
                }
            }],

            legend: {
                enabled: false
            },
        }
        return(
            <HighchartsReact
                highcharts={Highcharts}
                options={options}
                callback={this.masterCallBack}
            />
        );
    }
}

export { CreateHighChart };




