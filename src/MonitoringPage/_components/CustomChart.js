import React from 'react';
import { VictoryChart, VictoryAxis } from 'victory';
import { VictoryLine, VictoryScatter } from 'victory';
var CircularBuffer = require("circular-buffer");

class CustomChart extends React.Component {
    constructor(props) {
        super(props);
        this.buff = new CircularBuffer(20);
        this.data = {x: '', y: -1};
    }

    componentDidUpdate() {
        this.data = {x: this.props.time, y: this.props.value};
        this.buff.push(this.data);
    }

    render() {
        return(
        <VictoryChart height={300} animate={{ duration: 500}} >
            <VictoryAxis
                style = {{ axis: { stroke: '#c43a31' },
                    tickLabels: { fontSize: 10, fill: '#c43a31', angle: 45},
                }}  />
            <VictoryAxis
                style={{ axis: { stroke: '#c43a31' },
                    tickLabels: { fontSize: 10, fill: '#c43a31', fontWeight: 'bold' }
                    }} dependentAxis/>
            <VictoryLine 
                interpolation="monotoneX" data={this.buff.toarray()}
                style={{ data: { stroke: "#c43a31" } }}/>
            <VictoryScatter data={this.buff.toarray()}
                labels={ (d) => Number(d.y.toFixed(2)) }
                style={{ data: { fill: "#c43a31", } }} />
        </VictoryChart>
        );
    }
}

export { CustomChart }