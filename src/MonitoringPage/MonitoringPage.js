import React from 'react';
import { AppBar, Toolbar, Typography } from '@material-ui/core';
import { Client } from 'paho-mqtt';

import { MapContainer, CreateHighChart } from './_components';
import { history } from '../_helpers';
import { riderService } from '../_services';

class MonitoringPage extends React.Component {
    constructor(props) {
        super(props);
        this.client = null;
        this.state = {
            rideSelected: true,
            riderTopic: null,
            payLoad: 'false',

            speedArray: [],
            cadenceArray: [],
            powerArray: [],
            positionArray: [],

            totalDistance: 0
        }
        this.updatePayload = this.updatePayload.bind(this);
        this.initClient = this.initClient.bind(this);
        this.stopClient = this.stopClient.bind(this);
        this.onConnect = this.onConnect.bind(this);
        this.onConnectionLost = this.onConnectionLost.bind(this);
        this.onMessageArrived = this.onMessageArrived.bind(this);
        this.getDistance = this.getDistance.bind(this);
    }

    initClient() {
        this.client = new Client('connectedbike.cf', Number(8083), String(Date.now()));

        this.client.onConnectionLost = this.onConnectionLost;
        this.client.onMessageArrived = this.onMessageArrived;

        this.client.connect({
            userName: "george",
            password: "Georgebike96",
            onSuccess: this.onConnect
        });
    }

    stopClient() {
        if (this.client !== null) {
            this.client.unsubscribe(`connectedbike/ridedata/${this.state.riderTopic}`);
            this.client.disconnect();
        }
    }

    onConnect() {
        this.client.subscribe(`connectedbike/ridedata/${this.state.riderTopic}`); // Concat rider specific topic to 'connectedbike' to create the full topic path
    }

    onConnectionLost(responseObject) {
        if (responseObject.errorCode !== 0) {
            console.log("onConnectionLost: " + responseObject.errorMessage);
        }
    }

    onMessageArrived(message) {
        let newPayload = JSON.parse(message.payloadString);
        let currentMarker = { lat: Math.round(newPayload.latitude * 1000000) / 1000000, lng: Math.round(newPayload.longitude * 1000000) / 1000000 }
        let prevMarker = { lat: Math.round(this.state.payLoad.latitude * 1000000) / 1000000, lng: Math.round(this.state.payLoad.longitude * 1000000) / 1000000 }
        this.getDistance(prevMarker, currentMarker, true);    // compute the distance between current and prev pos. markers and add the value to the total distance

        this.updatePayload(JSON.parse(message.payloadString));
    }

    updatePayload(newPayload) {
        delete newPayload['ride_id']
        this.setState({
            payLoad: newPayload,
        });
    }

    rad = function (x) {
        return x * Math.PI / 180;
    }

    getDistance(p1, p2, isMqtt) {
        var R = 6378137; // Earth’s mean radius in meter
        var dLat = this.rad(p2.lat - p1.lat);
        var dLong = this.rad(p2.lng - p1.lng);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.rad(p1.lat)) * Math.cos(this.rad(p2.lat)) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        var d_Km = d / 1000;                    // convert meters to km
        d_Km = parseFloat(d_Km.toFixed(3));     // reduce float value to 3 decimals

        if (isMqtt && !isNaN(d_Km)) {
            let totalDist = this.state.totalDistance;
            totalDist += d_Km
            totalDist = parseFloat(totalDist.toFixed(3));   // reduce float value of totalDist to 3 decimals

            this.setState({ totalDistance: totalDist })

        } else { return d; }
    }

    getTotalDistance(positionArray) {
        let distance = 0;
        for (let i = 0; i < positionArray.length - 1; i++) {
            if(positionArray[i].position.lat === 0 && positionArray[i].position.lng === 0) continue;
            distance = distance + this.getDistance(positionArray[i].position, positionArray[i + 1].position, false);
        }
        this.setState({ totalDistance: distance })
    }

    componentWillUnmount() {
        if (this.client) {
            this.client.disconnect();
            this.client = null;
        }
    }

    componentWillMount() {

        if (this.props.location.state) {
            const selectedRideId = this.props.location.state.rideId;
            const selectedRiderTopic = this.props.location.state.riderTopic;    // send state from trainer page when chosing a rider
            if (selectedRideId) {
                riderService.getRideData(selectedRideId)
                    .then(
                        data => {
                            this.setState({
                                rideSelected: true,
                                speedArray: data.sort((element1, element2) => 
                                    { return new Date(element1.timestamp).getTime() - new Date(element2.timestamp).getTime() })
                                    .map(item => { return [new Date(item.timestamp).getTime(), parseFloat(item.speed)/10] }),
                                cadenceArray: data.sort((element1, element2) => 
                                    { return new Date(element1.timestamp).getTime() - new Date(element2.timestamp).getTime() })
                                    .map(item => { return [new Date(item.timestamp).getTime(), item.cadence] }),
                                powerArray: data.sort((element1, element2) => 
                                    { return new Date(element1.timestamp).getTime() - new Date(element2.timestamp).getTime() })
                                    .map(item => { return [new Date(item.timestamp).getTime(), item.power] }),
                                positionArray: data.sort((element1, element2) => 
                                    { return new Date(element1.timestamp).getTime() - new Date(element2.timestamp).getTime() })
                                    .map(item => { var date = new Date(item.timestamp); 
                                        return {position: { lat: (item.latitude * 1000000) / 1000000, lng: (item.longitude * 1000000) / 1000000 }, 
                                        time: date.getHours().toString()+":"+ date.getMinutes().toString()+":"+date.getSeconds().toString()} }),
                            }, () => { this.getTotalDistance(this.state.positionArray); });
                        },
                        error => alert(error)
                    );
            } else if (selectedRiderTopic) {
                this.setState({ rideSelected: false, riderTopic: selectedRiderTopic });
                this.initClient();
            }
        } else {
            history.push('/login');
        }
    }

    render() {
        if (this.state.rideSelected) {
            var speedArray = this.state.speedArray;
            var cadenceArray = this.state.cadenceArray;
            var powerArray = this.state.powerArray;
            var positionArray = this.state.positionArray;
        }

        return (
            <div>
                <div className='row'>
                    <AppBar style={{ position: "fixed", left: 0, top: 0 }}>
                        <Toolbar>
                            <Typography variant="h5" color={"textPrimary"} noWrap>
                                Rider's Activity Monitoring Screen
                            </Typography>
                        </Toolbar>
                    </AppBar>
                </div>
                <div className="row">
                    <div className="col-md-4 offset-md-0" style={{ height: 'calc(100% - 64px)', top: 64, }}>
                        <h3 style={{ margin: 16, paddingLeft: 16 }}>
                            SPEED - Avg Speed: {this.state.payLoad.avgSpeed} [km/h]
                        </h3>
                        {this.state.rideSelected && <CreateHighChart rideSelected={true} data={speedArray} />}
                        {!this.state.rideSelected && <CreateHighChart rideSelected={false} data={[new Date(this.state.payLoad.timestamp).getTime(), parseFloat(this.state.payLoad.speed / 10)]} />}
                    </div>
                    <div className="col-md-4 offset-md-0" style={{ height: 'calc(100% - 64px)', top: 64, }}>
                        {speedArray === [] &&
                            <img style={{ margin: 32 }} alt="loading" src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
                        <h3 style={{ margin: 16, paddingLeft: 16 }}>
                            CADENCE - Avg Cadence: {this.state.payLoad.avgCadence} [RPM]
                        </h3>
                        {this.state.rideSelected && <CreateHighChart rideSelected={true} data={cadenceArray} />}
                        {!this.state.rideSelected && <CreateHighChart rideSelected={false} data={[new Date(this.state.payLoad.timestamp).getTime(), this.state.payLoad.cadence]} />}
                    </div>
                    <div className="col-md-4 offset-md-0" style={{ height: 'calc(100% - 64px)', top: 64, }}>
                        <h3 style={{ margin: 16, paddingLeft: 16 }}>
                            POWER - Avg Power: {this.state.payLoad.avgPower} [watts]
                        </h3>
                        {this.state.rideSelected && <CreateHighChart rideSelected={true} data={powerArray} />}
                        {!this.state.rideSelected && <CreateHighChart rideSelected={false} data={[new Date(this.state.payLoad.timestamp).getTime(), this.state.payLoad.power]} />}
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 offset-md-0" style={{ height: 'calc(100% - 64px)', top: 64 }}>
                        <h3 style={{ margin: 16, paddingLeft: 16 }}>
                            POSITION - Total Distance: { parseFloat( (this.state.totalDistance/1000).toFixed(3) ) } KM
                        </h3>
                        {this.state.rideSelected && <MapContainer rideSelected={true} positionArray={positionArray} />}
                        {!this.state.rideSelected && <MapContainer lat={Math.round(this.state.payLoad.latitude * 1000000) / 1000000} lng={Math.round(this.state.payLoad.longitude * 1000000) / 1000000} />}
                    </div>
                </div>
            </div>
        );
    }
}

export { MonitoringPage }