import { riderConstants, userConstants } from '../_constants';

export function rider(state = { screenComponent: [] }, action) {
    switch (action.type) {
        case riderConstants.GETRIDES_REQUEST:
            return Object.assign({}, state, { loading: true });
        case riderConstants.GETRIDES_SUCCESS:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return Object.assign({}, withoutLoading, { rides: action.rides });
            }
            return Object.assign({}, state, { rides: action.rides });
        case riderConstants.GETRIDES_FAILURE:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return Object.assign({}, withoutLoading, { error: action.error });
            }
            return Object.assign({}, state, { error: action.error });
        case riderConstants.SELECT_RIDE:
            return Object.assign({}, state, { selectedRideId: action.rideId })

        ////////////////////////////////////
        case riderConstants.DELETERIDE_REQUEST:
            return Object.assign({}, state, { loading: true });
        case riderConstants.DELETERIDE_SUCCESS:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return Object.assign({}, withoutLoading, { rides: withoutLoading.rides.filter(ride => ride.id !== action.rideId) });
            }
            return Object.assign({}, state, { rides: state.rides.filter(ride => ride.id !== action.rideId) });
        case riderConstants.DELETERIDE_FAILURE:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return withoutLoading;
            }
            return state;

        ////////////////////////////////////
        case riderConstants.GETALLTRAINERS_REQUEST:
            return Object.assign({}, state, { loading: true });
        case riderConstants.GETALLTRAINERS_SUCCESS:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return Object.assign({}, withoutLoading, { allTrainers: action.trainers });
            }
            return Object.assign({}, state, { allTrainers: action.trainers });
        case riderConstants.GETALLTRAINERS_FAILURE:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return Object.assign({}, withoutLoading, { error: action.error });
            }
            return Object.assign({}, state, { error: action.error });

        ////////////////////////////////////
        case riderConstants.GETCONNERCTEDTRAINERS_REQUEST:
            return Object.assign({}, state, { loading: true });
        case riderConstants.GETCONNECTEDTRAINERS_SUCCESS:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return Object.assign({}, withoutLoading, { connectedTrainers: action.trainers });
            }
            return Object.assign({}, state, { connectedTrainers: action.trainers });
        case riderConstants.GETCONNECTEDTRAINERS_FAILURE:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return Object.assign({}, withoutLoading, { error: action.error });
            }
            return Object.assign({}, state, { error: action.error });

        //////////////////////////////////////
        case riderConstants.BEGINCOLLAB_REQUEST:
            return Object.assign({}, state, { loading: true });
        case riderConstants.BEGINCOLLAB_SUCCESS:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return withoutLoading;
            }
            return state;
        case riderConstants.BEGINCOLLAB_FAILURE:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return withoutLoading;
            }
            return state;

        ////////////////////////////////////
        case riderConstants.PUSH_SCREEN_COMPONENT:
            return Object.assign({}, state, { screenComponent: action.Component })
        case userConstants.LOGOUT:
            return {};
        default:
            return state;
    }
}