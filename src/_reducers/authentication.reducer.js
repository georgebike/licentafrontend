import { userConstants } from '../_constants';

export function authentication(state = {loading: true}, action) {
  switch (action.type) {
    case userConstants.GETINFO_REQUEST:
      return Object.assign({}, state, { loading: true });
    case userConstants.GETINFO_SUCCESS:
      if (state.loading) {
        const { loading, error, ...withoutLoading } = state;
        return Object.assign({}, withoutLoading, {user_info: action.user_info})
      }
      return Object.assign({}, state, { user_info: action.user_info })  
    case userConstants.GETINFO_FAILURE:
      if (state.loading) {
        const { loading, error, ...withoutLoading } = state;
        return Object.assign({}, withoutLoading, {error: action.error})
      }
      return Object.assign({}, state, { error: action.error })

    case userConstants.LOGIN_REQUEST:
      return Object.assign({}, state, { loggingIn: true });
    case userConstants.LOGIN_SUCCESS:
      if (state.loggingIn) {
        const { loggingIn, ...withoutLoggingIn } = state;
        return Object.assign({}, withoutLoggingIn, { loggedIn: true, user: action.user });
      }
      return Object.assign({}, state, { loggedIn: true, user: action.user });
    case userConstants.LOGIN_FAILURE:
      return {};
    case userConstants.LOGOUT:
      return {};
    default:
      return state
  }
}