// must combine reducers into rootReducer here
import { combineReducers } from 'redux';
import { alert } from './alert.reducer';
import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './user.reducer';
import { rider } from './rider.reducer';
import { trainer } from './trainer.reducer';

export default combineReducers({
    alert,
    authentication,
    registration,
    users,
    rider,
    trainer
});