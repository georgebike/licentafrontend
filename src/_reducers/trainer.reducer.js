import { trainerConstants, userConstants } from '../_constants';

export function trainer(state = { screenComponent: [] }, action) {
    switch (action.type) {
        case trainerConstants.GETCONNERCTEDRIDERS_REQUEST:
            return Object.assign({}, state, { loading: true });
        case trainerConstants.GETCONNECTEDRIDERS_SUCCESS:
            if (state.loading) {
                const { loading, error, ...withoutLoading } = state;
                return Object.assign({}, withoutLoading, { connectedRiders: action.riders });
            }
            return Object.assign({}, state, { connectedRiders: action.riders });
        case trainerConstants.GETCONNECTEDRIDERS_FAILURE:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return Object.assign({}, withoutLoading, { error: action.error });
            }
            return Object.assign({}, state, { error: action.error });

        //////////////////////////////////////
        case trainerConstants.GETALLRIDERS_REQUEST:
            return Object.assign({}, state, { loading: true });
        case trainerConstants.GETALLRIDERS_SUCCESS:
            if (state.loading) {
                const { loading, error, ...withoutLoading } = state;
                return Object.assign({}, withoutLoading, { allRiders: action.riders });
            }
            return Object.assign({}, state, { allRiders: action.riders });
        case trainerConstants.GETALLRIDERS_FAILURE:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return Object.assign({}, withoutLoading, { error: action.error });
            }
            return Object.assign({}, state, { error: action.error });

        //////////////////////////////////////
        case trainerConstants.BEGINCOLLAB_REQUEST:
            return Object.assign({}, state, { loading: true });
        case trainerConstants.BEGINCOLLAB_SUCCESS:
            if (state.loading) {
                const { loading, error, ...withoutLoading } = state;
                return withoutLoading;
            }
            return state;
        case trainerConstants.BEGINCOLLAB_FAILURE:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return withoutLoading;
            }
            return state;

        //////////////////////////////////////
        case trainerConstants.ENDCOLLAB_REQUEST:
            return Object.assign({}, state, { loading: true });
        case trainerConstants.ENDCOLLAB_SUCCESS:
            if (state.loading) {
                const { loading, error, ...withoutLoading } = state;
                return withoutLoading;
            }
            return state;
        case trainerConstants.ENDCOLLAB_FAILURE:
            if (state.loading) {
                const { loading, ...withoutLoading } = state;
                return withoutLoading;
            }
            return state;

        //////////////////////////////////////
        case trainerConstants.GETRIDES_REQUEST:
            return Object.assign({}, state, { loading: true })
        case trainerConstants.GETRIDES_SUCCESS:
            if (state.loading) {
                const { loading, error, ...withoutLoading } = state;        // new state will contain rides without loading and error fields
                return Object.assign({}, withoutLoading, { rides: action.rides });
            }
            return Object.assign({}, state, { rides: action.rides });
        case trainerConstants.GETRIDES_FAILURE:
            if (state.loading) {
                const { loading, rides, ...withoutLoading } = state;               // new state will contain only error without previous rides and loading fields
                return Object.assign({}, withoutLoading, { error: action.error });
            }
            return Object.assign({}, state, { error: action.error });

        //////////////////////////////////////
        case trainerConstants.PUSH_SCREEN_COMPONENT:
            return Object.assign({}, state, { screenComponent: action.Component })

        case userConstants.LOGOUT:
            return {};
        default:
            return state;
    }
}