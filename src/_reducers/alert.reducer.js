import { alertConstants } from '../_constants';

export function alert(state = {}, action) {
  switch (action.type) {
    case alertConstants.SUCCESS:
      return Object.assign({}, state, { type: 'alert-success', message: action.message });
    case alertConstants.ERROR:
      return Object.assign({}, state, { type: 'alert-error', message: action.message });
    case alertConstants.CLEAR:
      if (state.message && state.type){
        const { message, type, ...withoutAlert } = state;
        return withoutAlert;
      }
      return {};
    default:
      return state
  }
}