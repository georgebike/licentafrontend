import { authHeader } from '../_helpers';

export const riderService = {
    getRides,
    getRideData,
    getAllTrainers,
    getConnectedTrainers,
    beginCollab,
    endCollab,
    deleteRide
};

function getRides() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
    };

    let user = JSON.parse(localStorage.getItem('user'));

    return fetch(`https://connectedbike.cf/api/ride/${user.id}`, requestOptions).then(handleResponse);
}

function getRideData(rideId) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    }
    
    return fetch(`https://connectedbike.cf/api/ride/data/${rideId}`, requestOptions).then(handleResponse);
}

function deleteRide(rideId){
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader(),
    };

    return fetch(`https://connectedbike.cf/api/ride/${rideId}`, requestOptions).then(handleResponse);
}

function getAllTrainers() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
    };

    return fetch('https://connectedbike.cf/api/user/all', requestOptions).then(handleResponse)
}

function getConnectedTrainers() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
    };

    return fetch('https://connectedbike.cf/api/user/connected', requestOptions).then(handleResponse)
}

function beginCollab(userId) {
    const requestOptions = {
        method: 'PUT',
        headers: authHeader(),
    };

    return fetch(`https://connectedbike.cf/api/user/bind/${userId}`, requestOptions).then(handleResponse)
}

function endCollab(userId) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader(),
    };

    return fetch(`https://connectedbike.cf/api/user/bind/${userId}`, requestOptions).then(handleResponse)
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 (unauthorized) response returned from api
                logout();
                window.location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}