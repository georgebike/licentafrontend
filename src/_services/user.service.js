import { authHeader } from '../_helpers';

export const userService = {
    login,
    logout,
    register,
    add_info,
    get_info,
    update,
    delete: _delete
};

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    return fetch('https://connectedbike.cf/api/user/login', requestOptions)
        .then(handleResponse);
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

function get_info() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
    };

    return fetch('https://connectedbike.cf/api/user/', requestOptions).then(handleResponse)
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch('https://connectedbike.cf/api/user/', requestOptions).then(handleResponse);
}

function add_info(user_info, apitoken) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
                    'api-token': apitoken },
        body: JSON.stringify(user_info)
    };

    return fetch('https://connectedbike.cf/api/user/info', requestOptions).then(handleResponse);
}

function update(user_info) {
    const requestOptions = {
        method: 'PUT',
        headers: authHeader(),
        body: JSON.stringify(user_info)
    };

    return fetch('https://connectedbike.cf/api/user/info', requestOptions).then(handleResponse)
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete() {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch('https://connectedbike.cf/api/user/', requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                window.location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}