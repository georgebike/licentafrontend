import { userService } from './user.service';
import { riderService } from './rider.service';
import { trainerService } from './trainer.service';

export { userService, riderService, trainerService };