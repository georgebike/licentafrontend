import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import rootReducer from '../_reducers';

const loggerMiddleware = createLogger();

const persistConfig = {
    key: 'state',
    storage,
    blacklist: ['rider', 'trainer']
};

const peristedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
    peristedReducer,
    compose(
        applyMiddleware(
            thunkMiddleware,
            loggerMiddleware
        ),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__({ trace: true })
    )
);

export const persistor = persistStore(store);