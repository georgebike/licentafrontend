import { store } from './store';

export function authHeader() {
    const state = store.getState();

    const jwt_token = state.authentication.user.token

    if (jwt_token){
        return { 'Content-Type': 'application/json', 'api-token': jwt_token };
    }
}