import React, { Component } from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import './App.css';

import { history } from './_helpers';
import { alertActions } from './_actions';
import { PrivateRouteRider, PrivateRouteTrainer, PrivateRouteAdmin, PrivateRoute, AlertBar } from './_components';
import { RiderPage } from './RiderPage';
import { TrainerPage } from './TrainerPage';
import { LoginPage } from './LoginPage';
import { RegisterPage } from './RegisterPage';
import { AdminPage } from './AdminPage';
import { MonitoringPage } from './MonitoringPage';

import { store } from './_helpers';

class App extends Component {
  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    history.listen((location, action) => {
        // clear alert on location change
        dispatch(alertActions.clear());
    });
  }

  clearAlert = () => {this.props.dispatch(alertActions.clear())};

  render() {
    const { alert } = this.props;
    const state = store.getState();
    console.log(state);
    return (
        <div>
            {alert.message &&
                <AlertBar clearAlert={this.clearAlert} message={alert.message} />
            }
            <Router history={history}>
                <div>
                    <PrivateRouteRider exact path="/rider" component={RiderPage} />
                    <PrivateRouteTrainer exact path="/trainer" component={TrainerPage} />
                    <PrivateRouteAdmin exact path="/" component={AdminPage} />
                    <PrivateRoute exact path="/monitoring" component={MonitoringPage} />
                    <Route path="/login" component={LoginPage} />
                    <Route path="/register" component={RegisterPage} />
                </div>
            </Router>
        </div>
    );
  }
}

function mapStateToProps(state) { 
  const { alert } = state;
  return {
    alert
  };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App };
