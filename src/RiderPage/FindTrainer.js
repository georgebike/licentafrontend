import React from 'react';

import { riderActions } from '../_actions';

import { Button, ListItemText, Typography, List, ListItem } from '@material-ui/core';
import { connect } from 'react-redux';

class FindTrainer extends React.Component {
    componentDidMount() {
        console.log(this.props);
        const { rider } = this.props

        if (!rider.allTrainers) {
            this.props.dispatch(riderActions.getAllTrainers());
        }
    }

    beginCollab(user_id, e) {
        this.props.dispatch(riderActions.beginCollab(user_id));
    }

    render() {
        const { rider } = this.props;
        return(
            <div>
                {rider.allTrainers &&
                    <List style={{width: '100%', maxWidth: 720}}>
                        {rider.allTrainers.map((trainer, index) =>
                            <ListItem key={index} className="jumbotron" alignItems="flex-start">
                                <ListItemText primary={`${trainer.first_name} ${trainer.last_name}`}
                                    secondary={
                                        <React.Fragment>
                                            <Typography component="span" style={{display: 'inline'}} color="textPrimary">
                                            <b>DISCIPLINE: </b>{trainer.discipline} <b>LEVEL: </b>{trainer.level}
                                            </Typography> <br />
                                            <b>CERTIFICATE : </b>{trainer.certificate}
                                        </React.Fragment>
                                    }
                                />
                                <Button color="primary" onClick={this.beginCollab.bind(this, trainer.user_id)} > Connect </Button>
                            </ListItem>
                        )}
                    </List>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { rider } = state;

    return {
        rider
    };
}

const connectedFindTrainer = connect(mapStateToProps)(FindTrainer);
export { connectedFindTrainer as FindTrainer };