import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';

import { riderActions } from '../_actions';

class MyRides extends React.Component {
    componentDidMount() {
        const { rider } = this.props;
        /* dispatch getRides only the first time the component mounts; after this we have the rides stored in the redux store */
        if (!rider.connectedTrainers) {
            this.props.dispatch(riderActions.getConnectedTrainers());
        }

        if (!rider.rides) {
            this.props.dispatch(riderActions.getRides());
        }
    }

    deleteRide = (rideId, e) => {
        this.props.dispatch(riderActions.deleteRide(rideId));
    }

    endCollab = (user_id, e) => {
        this.props.dispatch(riderActions.endCollab(user_id));
    }

    render() {
        const { user, rider } = this.props;
        return (
            <div >
                <h1>Hi {user.username}!</h1>
                <p>Here you can find all the trainers you're connected with as well as all your previous rides</p>
                <h3>Connected Trainers:</h3>
                {rider.connectedTrainers &&
                    <ul>
                        {rider.connectedTrainers.map((trainer, index) =>
                            <li key={trainer.user_id} >
                                {trainer.first_name} {trainer.last_name}
                                <Button color="primary" style={{margin: 8}} onClick={this.endCollab.bind(this, trainer.user_id)} > End collaboration </Button>
                            </li>
                        )}
                    </ul>
                }
                <h3>All RIDES:</h3>
                {rider.loading && <em>Loading rides...</em>}
                {rider.rides &&
                    <ul>
                        {rider.rides.map((ride, index) =>
                            <li key={ride.id} >
                                <Link to={{ pathname: "/monitoring", state: { rideId: ride.id } }} className="btn btn-link">
                                    Ride id: {ride.id}  From: {ride.date}
                                </Link>
                                <Button color="primary" onClick={this.deleteRide.bind(this, ride.id)} > Delete Ride </Button>
                            </li>
                        )}
                    </ul>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { rider, authentication } = state;
    const { user } = authentication;
    return {
        user,
        rider
    };
}

const connectedMyRides = connect(mapStateToProps)(MyRides);
export { connectedMyRides as MyRides };