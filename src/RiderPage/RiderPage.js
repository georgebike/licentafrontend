import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { riderActions } from '../_actions';
import { MyRides } from './index';
import { PersistentDrawer } from './_components';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';


class RiderPage extends React.Component {
    // before mounting the component push the initial component to redux state that will be rendered inside RiderPage
    componentDidMount() {
        this.props.riderActions.pushScreenComponent(<MyRides />);
    }

    render() {
        const { rider, user } = this.props;
        return (    // send the redux action down to Persistent drawer as props
            <div>
                <AppBar style={{position:"fixed", left:0, top:0}}>
                    <Toolbar>
                    <Typography variant="h5" color={"textPrimary"}  noWrap>
                        You are logged in as {user.username}
                    </Typography>
                    </Toolbar>
                </AppBar>
                <div className="row" >
                    <div className="col-sm-3 col-md-2" style={{ height: 'calc(100% - 64px)', top: 64, }}>
                        <PersistentDrawer pushScreenComponent={this.props.riderActions.pushScreenComponent}/>
                    </div>
                    <div className="col-sm-9 col-md-9 col-md-offset-1" style={{ height: 'calc(100% - 64px)', top: 64, }}>
                        {rider.screenComponent}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { rider, authentication } = state;
    const { user } = authentication;
    return {
        user,
        rider
    };
}

function mapDispatchToProps(dispatch) {
    return { riderActions: bindActionCreators(riderActions, dispatch) }
}

const connectedRiderPage = connect(mapStateToProps, mapDispatchToProps)(RiderPage);
export { connectedRiderPage as RiderPage };