import React from 'react';
import { Link } from 'react-router-dom';

import { Drawer } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';

import { MyRides } from '../MyRides';
import { MyAccount } from '../MyAccount';
import { FindTrainer } from '../FindTrainer';

const styles = theme => ({
    drawer: {
        width: "15%",
        height: 'calc(100% - 64px)',
        top: 64,
        flexShrink: 0,
    },
    drawerPaper: {
        width: "15%",
        height: 'calc(100% - 64px)',
        top: 64,
    }
});

class PersistentDrawer extends React.Component {
    constructor(props){
        super(props);

        this.triggerMyRidesComponent = this.triggerMyRidesComponent.bind(this);
        this.triggerFindTrainerComponent = this.triggerFindTrainerComponent.bind(this);
        this.triggerMyAccountComponent = this.triggerMyAccountComponent.bind(this);
    }
    
    triggerMyRidesComponent(){
        this.props.pushScreenComponent(<MyRides />)
    }
    triggerFindTrainerComponent(){
        this.props.pushScreenComponent(<FindTrainer />)
    }
    triggerMyAccountComponent(){
        this.props.pushScreenComponent(<MyAccount />)
    }

    render() {
        const { classes } = this.props;
        return (
            <Drawer className={classes.drawer} variant="persistent" anchor="left" open={true} classes={{ paper: classes.drawerPaper, }}>
                <List>
                    <ListItem button key="rides" onClick={this.triggerMyRidesComponent}>
                        <ListItemIcon><InboxIcon /></ListItemIcon>
                        <ListItemText primary="My rides" />
                    </ListItem>
                    <ListItem button key="trainer" onClick={this.triggerFindTrainerComponent}>
                        <ListItemIcon><InboxIcon /></ListItemIcon>
                        <ListItemText primary="Find Trainer" />
                    </ListItem>
                    <ListItem button key="account" onClick={this.triggerMyAccountComponent}>
                        <ListItemIcon><InboxIcon /></ListItemIcon>
                        <ListItemText primary="My account" />
                    </ListItem>
                    <ListItem button component={Link} to="/login" key="logout">
                        <ListItemText primary="Logout" />
                    </ListItem>
                </List>
            </Drawer>
        );
    }
}
let styledDrawer = withStyles(styles)(PersistentDrawer);
export { styledDrawer as PersistentDrawer }