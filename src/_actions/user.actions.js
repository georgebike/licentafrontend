import { userConstants } from '../_constants';
import { userService } from '../_services';
import { alertActions } from './alert.actions';
import { history } from '../_helpers';

export const userActions = {
    login,
    logout,
    get_info,
    update,
    register,
    delete: _delete
};

function login(username, password) {
    return dispatch => {
        dispatch(request());

        userService.login(username, password)
            .then(
                user => {                    
                    dispatch(success(user));

                    // store user details (without token) in local storage to keep user logged in between page refreshes
                    const { token, ...withoutToken } = user;
                    let storageUser = withoutToken;
                    localStorage.setItem('user', JSON.stringify(storageUser));

                    switch(user.role){
                        case 'R':
                            history.push('/rider');
                            break;
                        case 'T':
                            history.push('/trainer');
                            break;
                        case 'A':
                            history.push('/');
                            break;
                        default:
                            history.push('/login');
                            break;
                    } 
                },
                error => {
                    dispatch(failure());
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request() { return { type: userConstants.LOGIN_REQUEST } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure() { return { type: userConstants.LOGIN_FAILURE } }
}

function update(user_info) {
    return dispatch => {
        userService.update(user_info)
            .then(
                updated_user => { dispatch(alertActions.success("User updated successfully")) },
                error => { dispatch(alertActions.error(error.error)); }
            );
    };
}

function get_info() {
    return dispatch => {
        dispatch(request());

        userService.get_info()
            .then(
                info => dispatch(success(info)),
                error => { dispatch(failure(error.toString())); dispatch(alertActions.error(error)); }
            );
    };

    function request() { return { type: userConstants.GETINFO_REQUEST } }
    function success(user_info) { return { type: userConstants.GETINFO_SUCCESS, user_info } }
    function failure(error) { return { type: userConstants.GETINFO_FAILURE, error } }
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}

function register(user, user_info) {
    return dispatch => {
        dispatch(request(user));

        // Perform user registration (user, pass, role)
        userService.register(user)
            .then(
                data => {
                    // If apitoken returned, perform adding user_info based on role
                    userService.add_info(user_info, data.jwt_token)
                        .then(
                            message => {
                                dispatch(success());
                                history.push('/login');
                                dispatch(alertActions.success('Registration successful'));
                            },
                            error => {
                                dispatch(failure(error.toString()));
                                dispatch(alertActions.error(error.toString()));
                            }
                        );
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request() { return { type: userConstants.REGISTER_REQUEST } }
    function success() { return { type: userConstants.REGISTER_SUCCESS } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function _delete() {
    return dispatch => {
        userService.delete()
            .then(
                message => { dispatch(alertActions.success("Account deleted!")); logout(); history.push('/login'); },
                error => dispatch(alertActions.error(error.message))
            );
    };
}