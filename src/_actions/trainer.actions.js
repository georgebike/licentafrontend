import { trainerConstants } from '../_constants';
import { trainerService } from '../_services';
import { alertActions } from './alert.actions';
// import { history } from '../_helpers';

export const trainerActions = {
    getConnectedRiders,
    getAllRiders,
    getRides,
    beginCollab,
    endCollab,
    pushScreenComponent
}

function getConnectedRiders() { 
    return dispatch => {
        dispatch(request());

        trainerService.getConnectedRiders()
            .then(
                riders => dispatch(success(riders)),
                error => dispatch(failure(error))
            );
    }

    function request() { return { type: trainerConstants.GETCONNERCTEDRIDERS_REQUEST } }
    function success(riders) { return { type: trainerConstants.GETCONNECTEDRIDERS_SUCCESS, riders } }
    function failure(error) { return { type: trainerConstants.GETCONNECTEDRIDERS_FAILURE, error } }
}

function getAllRiders() { 
    return dispatch => {
        dispatch(request());

        trainerService.getAllRiders()
            .then(
                riders => dispatch(success(riders)),
                error => dispatch(failure(error))
            );
    }

    function request() { return { type: trainerConstants.GETALLRIDERS_REQUEST } }
    function success(riders) { return { type: trainerConstants.GETALLRIDERS_SUCCESS, riders } }
    function failure(error) { return { type: trainerConstants.GETALLRIDERS_FAILURE, error } }
}

function getRides(userId) {
    return dispatch => {
        dispatch(request(userId));

        trainerService.getRides(userId)
            .then(
                rides => dispatch(success(rides)),
                error => {dispatch(alertActions.error(error)); dispatch(failure(error))}
            )
    }

    function request(userId) { return { type: trainerConstants.GETRIDES_REQUEST, userId } }
    function success(rides) { return { type: trainerConstants.GETRIDES_SUCCESS, rides } }
    function failure(error) { return { type: trainerConstants.GETRIDES_FAILURE, error } }
}

function beginCollab(userId) {
    return dispatch => {
        dispatch(request(userId));

        trainerService.beginCollab(userId)
            .then(
                message => {
                    dispatch(success());
                    dispatch(alertActions.success(message.message));
                },
                error => {
                    dispatch(failure());
                    dispatch(alertActions.error(error));
                }
            )
    }

    function request() { return { type: trainerConstants.BEGINCOLLAB_REQUEST } }
    function success() { return { type: trainerConstants.BEGINCOLLAB_SUCCESS } }
    function failure() { return { type: trainerConstants.BEGINCOLLAB_FAILURE } }
}

function endCollab(userId) {
    return dispatch => {
        dispatch(request(userId));

        trainerService.endCollab(userId)
            .then(
                message => {
                    dispatch(success());
                    dispatch(alertActions.success(message.message));
                },
                error => {
                    dispatch(failure());
                    dispatch(alertActions.error(error.message));
                }
            )
    }

    function request() { return { type: trainerConstants.ENDCOLLAB_REQUEST } }
    function success() { return { type: trainerConstants.ENDCOLLAB_SUCCESS } }
    function failure() { return { type: trainerConstants.ENDCOLLAB_FAILURE } }
}

function pushScreenComponent(Component) { 
    return { type: trainerConstants.PUSH_SCREEN_COMPONENT, Component }
}
