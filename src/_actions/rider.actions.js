import { riderConstants } from '../_constants';
import { riderService } from '../_services';
import { alertActions } from './alert.actions';

export const riderActions = {
    getRides,
    getConnectedTrainers,
    getAllTrainers,
    beginCollab,
    endCollab,
    selectRide,
    deleteRide,
    pushScreenComponent
};

/* Get a list of rider's rides */
function getRides() {
    return dispatch => {
        dispatch(request());

        riderService.getRides()
            .then(
                rides => dispatch(success(rides)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: riderConstants.GETRIDES_REQUEST } }
    function success(rides) { return { type: riderConstants.GETRIDES_SUCCESS, rides } }
    function failure(error) { return { type: riderConstants.GETRIDES_FAILURE, error } }
}

function deleteRide(rideId) {
    return dispatch => {
        dispatch(request());

        riderService.deleteRide(rideId)
            .then(
                message => {
                    dispatch(success(rideId));
                    dispatch(alertActions.success(message.message));
                },
                error => {
                    dispatch(failure())
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request() { return { type: riderConstants.DELETERIDE_REQUEST } }
    function success(rideId) { return { type: riderConstants.DELETERIDE_SUCCESS, rideId } }
    function failure() { return { type: riderConstants.DELETERIDE_FAILURE } }
}

function getAllTrainers() {
    return dispatch => {
        dispatch(request());

        riderService.getAllTrainers()
            .then(
                trainers => dispatch(success(trainers)),
                error => dispatch(failure(error))
            );
    }

    function request() { return { type: riderConstants.GETALLTRAINERS_REQUEST } }
    function success(trainers) { return { type: riderConstants.GETALLTRAINERS_SUCCESS, trainers } }
    function failure(error) { return { type: riderConstants.GETALLTRAINERS_FAILURE, error } }
}

function getConnectedTrainers() {
    return dispatch => {
        dispatch(request());

        riderService.getConnectedTrainers()
            .then(
                trainers => dispatch(success(trainers)),
                error => dispatch(failure(error))
            );
    }

    function request() { return { type: riderConstants.GETCONNERCTEDTRAINERS_REQUEST } }
    function success(trainers) { return { type: riderConstants.GETCONNECTEDTRAINERS_SUCCESS, trainers } }
    function failure(error) { return { type: riderConstants.GETCONNECTEDTRAINERS_FAILURE, error } }
}

function beginCollab(userId) {
    return dispatch => {
        dispatch(request(userId));

        riderService.beginCollab(userId)
            .then(
                message => {
                    dispatch(success());
                    dispatch(alertActions.success(message.message));
                },
                error => {
                    dispatch(failure());
                    dispatch(alertActions.error(error));
                }
            )
    }

    function request() { return { type: riderConstants.BEGINCOLLAB_REQUEST } }
    function success() { return { type: riderConstants.BEGINCOLLAB_SUCCESS } }
    function failure() { return { type: riderConstants.BEGINCOLLAB_FAILURE } }
}

function endCollab(userId) {
    return dispatch => {
        dispatch(request(userId));

        riderService.endCollab(userId)
            .then(
                message => {
                    dispatch(success());
                    dispatch(alertActions.success(message.message));
                },
                error => {
                    dispatch(failure());
                    dispatch(alertActions.error(error.message));
                }
            )
    }

    function request() { return { type: riderConstants.ENDCOLLAB_REQUEST } }
    function success() { return { type: riderConstants.ENDCOLLAB_SUCCESS } }
    function failure() { return { type: riderConstants.ENDCOLLAB_FAILURE } }
}

function pushScreenComponent(Component) { 
     return { type: riderConstants.PUSH_SCREEN_COMPONENT, Component }
}

function selectRide(rideId) {
    return { type: riderConstants.SELECT_RIDE, rideId }
}